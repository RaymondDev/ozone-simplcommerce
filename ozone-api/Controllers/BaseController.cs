﻿using AutoMapper;
using Core.Domain;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozone_api.Controllers
{
    public class BaseController : Controller
    {
        protected readonly OzoneDbContext dbContext;
        protected readonly IMapper mapper;

        public BaseController(OzoneDbContext context)
        {
            this.dbContext = context;
        }

        public BaseController(OzoneDbContext context, IMapper mapper)
        {
            this.dbContext = context;
            this.mapper = mapper;
        }
    }
}
