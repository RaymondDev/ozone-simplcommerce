﻿using AutoMapper;
using Core.Domain;
using Core.Models.ProductModels;
using Core.Services.ProductServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozone_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AttributeGroupController : BaseController
    {
        private readonly IAttributeGroupService _attributeGroupService;
        public AttributeGroupController(
            OzoneDbContext context,
           IAttributeGroupService attributeGroupService,
           IMapper mapper
           ) : base(context, mapper)
        {
            _attributeGroupService = attributeGroupService;
        }

        [HttpGet("GetAllAttributeGroups")]
        public IActionResult Get()
        {
            try
            {
                return Ok(_attributeGroupService.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("GetAttributeGroup")]
        public IActionResult Get(int Id)
        {
            try
            {
                return Ok(_attributeGroupService.Get(new ProductAttributeGroupDto { PkAttributeGroupId = Id }));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost("CreateAttributeGroup")]
        public IActionResult Create([FromBody]ProductAttributeGroupDto model)
        {
            try
            {
                return Ok(_attributeGroupService.Create(model));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPut("UpdateAttributeGroup")]
        public IActionResult Update([FromBody]ProductAttributeGroupDto model)
        {
            try
            {
                return Ok(_attributeGroupService.Update(model));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("DeleteAttributeGroup")]
        public IActionResult Delete(int Id)
        {
            try
            {
                _attributeGroupService.Delete(new ProductAttributeGroupDto { PkAttributeGroupId = Id });

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
