﻿using AutoMapper;
using Core.Domain;
using Core.Models.ProductModels;
using Core.Services.ProductServices;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozone_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : BaseController
    {
        private readonly IProductService _ProductService;
        public ProductController(
            OzoneDbContext context,
           IProductService ProductService,
           IMapper mapper
           ) : base(context, mapper)
        {
            _ProductService = ProductService;
        }

        [HttpGet("GetAllProducts")]
        public IActionResult Get()
        {
            try
            {
                return Ok(_ProductService.GetAll());
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet("GetProduct")]
        public IActionResult Get(int Id)
        {
            try
            {
                return Ok(_ProductService.Get(new ProductDto { PkProductId = Id }));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost("CreateProduct")]
        public IActionResult Create([FromBody]ProductDto model)
        {
            try
            {
                return Ok(_ProductService.Create(model));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPut("UpdateProduct")]
        public IActionResult Update([FromBody]ProductDto model)
        {
            try
            {
                return Ok(_ProductService.Update(model));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("DeleteProduct")]
        public IActionResult Delete(int Id)
        {
            try
            {
                _ProductService.Delete(new ProductDto { PkProductId = Id });

                return NoContent();
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
