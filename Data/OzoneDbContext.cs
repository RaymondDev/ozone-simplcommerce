﻿using Data.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data
{
    public class OzoneDbContext : DbContext
    {
        public OzoneDbContext(DbContextOptions<OzoneDbContext> options)
            : base(options)
        {
        }

        //public DbSet<Category> Categories { get; set; }

        //public DbSet<Customer> Customers { get; set; }

        //public DbSet<Employee> Employees { get; set; }

        //public DbSet<EmployeeTerritory> EmployeeTerritories { get; set; }


        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries<AuditInfo>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.CreatedBy = string.Empty;
                        entry.Entity.Created = DateTime.Now;
                        break;
                    case EntityState.Modified:
                        entry.Entity.LastModifiedBy = string.Empty;
                        entry.Entity.LastModified = DateTime.Now;
                        break;
                }
            }

            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(OzoneDbContext).Assembly);
        }
    }
}
