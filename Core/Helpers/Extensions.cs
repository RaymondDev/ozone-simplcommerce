﻿using Core.Domain.Entities.User;
using System.Collections.Generic;
using System.Linq;
namespace Core.Helpers
{
    /// <summary>
    /// Extension methods to add convenience methods and extra functionality to existing types in C#.
    /// </summary>
    public static class ExtensionMethods
    {
        public static IEnumerable<User> WithoutPasswords(this IEnumerable<User> users)
        {
            return users.Select(x => x.WithoutPassword());
        }

        public static User WithoutPassword(this User user)
        {
            user.Password = null;
            return user;
        }
    }
}
