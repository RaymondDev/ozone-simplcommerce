﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Helpers
{
    public static class GlobalConfiguration
    {
        public static string WebRootPath { get; set; }

        public static string ContentRootPath { get; set; }
    }
}
