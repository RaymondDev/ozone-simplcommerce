﻿using AutoMapper;
using Core.Domain.Entities;
using Core.Domain.Entities.ProductEntity;
using Core.Domain.Entities.User;
using Core.Models;
using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<RegisterModel, User>();
            CreateMap<UpdateModel, User>();

            // Attribute Groups
            CreateMap<ProductAttributeGroup, ProductAttributeGroupDto>();
            CreateMap<ProductAttributeGroupDto, ProductAttributeGroup>();

            // Attribute
            CreateMap<ProductAttribute, ProductAttributeDto>();
            CreateMap<ProductAttributeDto, ProductAttribute>();

            // Brand
            CreateMap<ProductBrand, ProductBrandDto>();
            CreateMap<ProductBrandDto, ProductBrand>();

            // Category
            CreateMap<ProductCategory, ProductCategoryDto>();
            CreateMap<ProductCategoryDto, ProductCategory>();

            // Price History
            CreateMap<ProductPriceHistory, ProductPriceHistoryDto>();
            CreateMap<ProductPriceHistoryDto, ProductPriceHistory>();

            // Tax Class
            CreateMap<ProductTaxClass, ProductTaxClassDto>();
            CreateMap<ProductTaxClassDto, ProductTaxClass>();

            // Products
            CreateMap<Product, ProductDto>();
            CreateMap<ProductDto, Product>();
        }
    }
}
