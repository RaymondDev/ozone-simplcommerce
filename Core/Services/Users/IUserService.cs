﻿using Core.Domain.Entities.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.Users
{
    public interface IUserService
    {
        User Authenticate(string username, string password);

        IEnumerable<User> GetAll();

        User GetById(int id);

        User Create(User user, string password);

        void Update(User user, string password = null);

        void Delete(int id);
    }
}
