﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductAttributeMappingServices
{
    public interface IAttributeMappingService
    {
        IList<ProductAttributeMappingDto> GetProductAttributeMappings(int productId);
        void AddOrDeleteAttributeMappings(List<ProductAttributeMappingDto> mappings);
    }
}
