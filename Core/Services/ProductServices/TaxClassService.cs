﻿using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Services.ProductServices
{
    public class TaxClassService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;
        public TaxClassService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public ProductTaxClassDto Create(ProductTaxClassDto model)
        {
            try
            {
                ValidateInputs(_dbContext, model);

                ProductTaxClass dbTaxClass = _mapper.Map<ProductTaxClass>(model);

                _dbContext.TaxClasses.Add(dbTaxClass);

                _dbContext.SaveChanges();

                model.PkTaxClassId = dbTaxClass.PkTaxClassId;

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductTaxClassDto Get(ProductTaxClassDto model)
        {
            try
            {
                ProductTaxClass dbEntity = RetrieveTaxClass(_dbContext, model);

                return _mapper.Map<ProductTaxClassDto>(dbEntity);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public IList<ProductTaxClassDto> GetAll()
        {
            try
            {
                List<ProductTaxClass> dbTaxClasses = _dbContext.TaxClasses.ToList();

                List<ProductTaxClassDto> taxClasses = _mapper.Map<List<ProductTaxClass>, List<ProductTaxClassDto>>(dbTaxClasses);

                return taxClasses.OrderBy(x => x.Name).ToList(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductTaxClassDto Update(ProductTaxClassDto model)
        {
            try
            {
                ProductTaxClass dbEntity = RetrieveTaxClass(_dbContext, model);

                _dbContext.Entry(dbEntity).State = EntityState.Detached;

                dbEntity = _mapper.Map<ProductTaxClass>(model);

                _dbContext.TaxClasses.Update(dbEntity);

                _dbContext.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public void Delete(ProductTaxClassDto model)
        {
            try
            {
                ProductTaxClass dbEntity = RetrieveTaxClass(_dbContext, model);

                _dbContext.TaxClasses.Attach(dbEntity);

                _dbContext.TaxClasses.Remove(dbEntity);

                _dbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        private void ValidateInputs(OzoneDbContext _dbContext, ProductTaxClassDto model)
        {
            // Check if ProductTaxClass was specified.
            if (model == null)
                throw new Exception($"No tax class specified.");

            // Check uniqueness of TaxClasses
            if (_dbContext.TaxClasses.Any(x => x.Name == model.Name))
                throw new Exception($"Tax class with name '{ model.Name }' already exists");
        }

        private ProductTaxClass RetrieveTaxClass(OzoneDbContext _dbContext, ProductTaxClassDto model)
        {
            try
            {
                if (model == null)
                    throw new Exception("Tax class not found");

                ProductTaxClass dbEntity = null;

                if (model.PkTaxClassId > 0)
                {
                    dbEntity = _dbContext.TaxClasses.Where(x => x.PkTaxClassId == model.PkTaxClassId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(model.Name) && dbEntity == null)
                {
                    dbEntity = _dbContext.TaxClasses.Where(x => x.Name == model.Name).FirstOrDefault();
                }

                if (dbEntity == null)
                    throw new Exception("Tax class does not exists");

                return dbEntity;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}
