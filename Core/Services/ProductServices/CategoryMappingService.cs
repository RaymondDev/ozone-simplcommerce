﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;

namespace Core.Services.ProductServices
{
    public class CategoryMappingService : ICategoryMappingService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;

        public CategoryMappingService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public void AddOrDeleteCategoryMappings(List<ProductCategoryMappingDto> mappings)
        {
            Product dbProduct = null;

            if (mappings.Any())
            {
                int first = mappings.First().FkProductId;

                if (mappings.Any(x => x.FkProductId != first))
                {
                    throw new Exception("Inconsistencies found on productId");
                }

                dbProduct = _dbContext.Products
                    .Where(x => x.PkProductId == mappings.First().FkProductId)
                        .FirstOrDefault();
            }

            if (dbProduct != null)
            {
                List<ProductCategoryMapping> newMappings = new List<ProductCategoryMapping>();
                List<int> newMappingsCategoryIds = mappings.Select(x => x.FkCategoryId).ToList();

                List<ProductCategoryMapping> existingDbmappings = _dbContext.ProductCategoryMappings
                    .Where(x => x.FkProductId == dbProduct.PkProductId)
                        .ToList();

                List<ProductCategoryMapping> mappingsToDelete = existingDbmappings
                    .Where(x => !newMappingsCategoryIds.Contains(x.FkCategoryId)).ToList();

                List<int> existingIds = existingDbmappings.Select(x => x.FkCategoryId).ToList();


                foreach (var map in mappings)
                {
                    // Check if we already have this mapping
                    if (existingIds.Contains(map.FkCategoryId))
                    {
                        continue;
                    }

                    var mapping = new ProductCategoryMapping
                    {
                        FkCategoryId = map.FkCategoryId,
                        FkProductId = map.FkProductId,
                        DisplayOrder = 1,
                        IsFeaturedProduct = true
                    };

                    newMappings.Add(mapping);
                }

                if (mappingsToDelete.Any())
                {
                    _dbContext.ProductCategoryMappings.RemoveRange(mappingsToDelete);
                }

                if (newMappings.Any())
                {
                    _dbContext.ProductCategoryMappings.AddRange(newMappings);
                }

                _dbContext.SaveChanges();
            }
        }

        public IList<ProductCategoryMappingDto> GetProductCategoryMappings(int productId)
        {
            try
            {
                Product dbProduct = null;
                List<ProductCategoryMapping> dbMappings = null;
                List<ProductCategoryMappingDto> mappings = null;

                // Validate if the product exists
                if (productId > 0)
                {
                    dbProduct = _dbContext.Products
                        .Where(x => x.PkProductId == productId)
                            .FirstOrDefault();
                }

                // Retrieve all product mappings
                if (dbProduct != null)
                {
                    dbMappings = _dbContext.ProductCategoryMappings
                        .Where(x => x.FkProductId == productId)
                            .ToList();
                }

                // Return mapping objects
                if (dbMappings.Any())
                {
                    Dictionary<int, string> dicCategories = _dbContext.Categories.ToDictionary(t => t.PkCategoryId, t => t.Name);

                    foreach (ProductCategoryMapping mapping in dbMappings)
                    {
                        ProductCategoryMappingDto map = new ProductCategoryMappingDto
                        {
                            PkProductMappingCategoryId = mapping.PkProductMappingCategoryId,
                            FkCategoryId = mapping.FkCategoryId,
                            IsFeaturedProduct = true,
                            DisplayOrder = 1
                        };

                        if (dicCategories.ContainsKey(mapping.FkCategoryId))
                        {
                            map.CategoryName = dicCategories[mapping.FkCategoryId];
                        }

                        else
                        {
                            map.CategoryName = "N/A";
                        }

                        mappings.Add(map);
                    }
                }

                return mappings;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

    }
}
