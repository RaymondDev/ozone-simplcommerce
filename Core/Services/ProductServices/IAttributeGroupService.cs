﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface IAttributeGroupService
    {
        IList<ProductAttributeGroupDto> GetAll();
        ProductAttributeGroupDto Get(ProductAttributeGroupDto model);
        ProductAttributeGroupDto Create(ProductAttributeGroupDto ProductAttributeGroup);
        ProductAttributeGroupDto Update(ProductAttributeGroupDto ProductAttributeGroup);
        void Delete(ProductAttributeGroupDto ProductAttributeGroup);
    }
}
