﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Microsoft.EntityFrameworkCore;

namespace Core.Services.ProductServices
{
    public class AttributeService : IAttributeService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;
        public AttributeService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public ProductAttributeDto Create(ProductAttributeDto model)
        {
            try
            {
                ValidateInputs(_dbContext, model);

                ProductAttribute dbAttribute = new ProductAttribute
                {
                    FkAttributeGroupId = model.FkAttributeGroupId,
                    Name = model.Name,
                };

                _dbContext.Attributes.Add(dbAttribute);
                _dbContext.SaveChanges();

                model.PkAttributeId = dbAttribute.PkAttributeId;
                model.Name = RetrieveGroupName(_dbContext, dbAttribute.FkAttributeGroupId);
                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public void Delete(ProductAttributeDto model)
        {
            try
            {
                ProductAttribute dbEntity = RetrieveProductAttribute(_dbContext, model);

                _dbContext.Attributes.Attach(dbEntity);

                _dbContext.Attributes.Remove(dbEntity);

                _dbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductAttributeDto Get(ProductAttributeDto model)
        {
            try
            {
                ProductAttribute dbEntity = RetrieveProductAttribute(_dbContext, model);

                ProductAttributeDto attribute = new ProductAttributeDto
                {
                    PkAttributeId = dbEntity.PkAttributeId,
                    FkAttributeGroupId = dbEntity.FkAttributeGroupId,
                    Name = dbEntity.Name,
                    GroupName = RetrieveGroupName(_dbContext, dbEntity.FkAttributeGroupId)
                };

                return attribute;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public IList<ProductAttributeDto> GetAll()
        {
            try
            {
                IList<ProductAttributeDto> attributes = new List<ProductAttributeDto>();

                foreach (ProductAttribute dbAttribute in _dbContext.Attributes)
                {
                    ProductAttributeDto attribute = new ProductAttributeDto
                    {
                        PkAttributeId = dbAttribute.PkAttributeId,
                        FkAttributeGroupId = dbAttribute.FkAttributeGroupId,
                        Name = dbAttribute.Name,
                        GroupName = RetrieveGroupName(_dbContext, dbAttribute.FkAttributeGroupId)
                    };

                    attributes.Add(attribute);
                }

                return attributes;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductAttributeDto Update(ProductAttributeDto model)
        {
            try
            {
                ProductAttribute dbEntity = RetrieveProductAttribute(_dbContext, model);

                _dbContext.Entry(dbEntity).State = EntityState.Detached;

                dbEntity = _mapper.Map<ProductAttribute>(model);

                _dbContext.Attributes.Update(dbEntity);

                _dbContext.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }


        /// <summary>
        /// Check uniqueness to avoid duplicates.
        /// </summary>
        private void ValidateInputs(OzoneDbContext _dbContext, ProductAttributeDto ProductAttribute)
        {
            // Check if ProductAttribute was specified.
            if (ProductAttribute == null)
                throw new Exception($"No ProductAttribute specified.");

            // Check uniqueness of ProductAttribute
            if (_dbContext.Attributes.Any(x => x.Name == ProductAttribute.Name))
                throw new Exception($"ProductAttribute { ProductAttribute.Name } already exists");
        }

        /// <summary>
        /// Retrieves entity using primary key or name
        /// </summary>
        private ProductAttribute RetrieveProductAttribute(OzoneDbContext _dbContext, ProductAttributeDto model)
        {
            try
            {
                if (model == null)
                    throw new Exception("ProductAttribute not found");

                ProductAttribute dbEntity = null;

                if (model.PkAttributeId > 0)
                {
                    dbEntity = _dbContext.Attributes.Where(x => x.PkAttributeId == model.PkAttributeId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(model.Name) && dbEntity == null)
                {
                    dbEntity = _dbContext.Attributes.Where(x => x.Name == model.Name).FirstOrDefault();
                }

                if (dbEntity == null)
                    throw new Exception("ProductAttribute does not exists");

                return dbEntity;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public string RetrieveGroupName(OzoneDbContext _dbContext, int fkGroupId)
        {
            if (_dbContext.AttributeGroups.Any(x => x.PkAttributeGroupId == fkGroupId))
            {
                return _dbContext.AttributeGroups
                    .Where(x => x.PkAttributeGroupId == fkGroupId)
                        .FirstOrDefault().Name;
            }

            else
            {
                return "N/A";
            }
        }
    }
}
