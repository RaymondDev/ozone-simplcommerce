﻿using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Services.ProductServices
{
    public class CategoryService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;
        public CategoryService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public ProductCategoryDto Create(ProductCategoryDto model)
        {
            try
            {
                ValidateInputs(_dbContext, model);

                ProductCategory dbCategory = _mapper.Map<ProductCategory>(model);

                _dbContext.Categories.Add(dbCategory);

                _dbContext.SaveChanges();

                model.PkCategoryId = dbCategory.PkCategoryId;

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductCategoryDto Get(ProductCategoryDto model)
        {
            try
            {
                ProductCategory dbEntity = RetrieveCategory(_dbContext, model);

                return _mapper.Map<ProductCategoryDto>(dbEntity);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public IList<ProductCategoryDto> GetAll()
        {
            try
            {
                List<ProductCategory> dbCategories = _dbContext.Categories.ToList();

                List<ProductCategoryDto> categorys = _mapper.Map<List<ProductCategory>, List<ProductCategoryDto>>(dbCategories);

                return categorys.OrderBy(x => x.Name).ToList(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductCategoryDto Update(ProductCategoryDto model)
        {
            try
            {
                ProductCategory dbEntity = RetrieveCategory(_dbContext, model);

                _dbContext.Entry(dbEntity).State = EntityState.Detached;

                dbEntity = _mapper.Map<ProductCategory>(model);

                _dbContext.Categories.Update(dbEntity);

                _dbContext.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public void Delete(ProductCategoryDto model)
        {
            try
            {
                ProductCategory dbEntity = RetrieveCategory(_dbContext, model);

                _dbContext.Categories.Attach(dbEntity);

                _dbContext.Categories.Remove(dbEntity);

                _dbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        private void ValidateInputs(OzoneDbContext _dbContext, ProductCategoryDto model)
        {
            // Check if ProductCategory was specified.
            if (model == null)
                throw new Exception($"No category specified.");

            // Check uniqueness of category
            if (_dbContext.Categories.Any(x => x.Name == model.Name))
                throw new Exception($"Category with name '{ model.Name }' already exists");
        }

        private ProductCategory RetrieveCategory(OzoneDbContext _dbContext, ProductCategoryDto model)
        {
            try
            {
                if (model == null)
                    throw new Exception("Category not found");

                ProductCategory dbEntity = null;

                if (model.PkCategoryId > 0)
                {
                    dbEntity = _dbContext.Categories.Where(x => x.PkCategoryId == model.PkCategoryId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(model.Name) && dbEntity == null)
                {
                    dbEntity = _dbContext.Categories.Where(x => x.Name == model.Name).FirstOrDefault();
                }

                if (dbEntity == null)
                    throw new Exception("Category does not exists");

                return dbEntity;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}
