﻿using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Services.ProductServices
{
    public class ProductBrandService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;
        public ProductBrandService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public ProductBrandDto Create(ProductBrandDto model)
        {
            try
            {
                ValidateInputs(_dbContext, model);

                ProductBrand dbBrand = _mapper.Map<ProductBrand>(model);

                _dbContext.Brands.Add(dbBrand);

                _dbContext.SaveChanges();

                model.PkBrandId = dbBrand.PkBrandId;

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductBrandDto Get(ProductBrandDto model)
        {
            try
            {
                ProductBrand dbEntity = RetrieveBrand(_dbContext, model);

                return _mapper.Map<ProductBrandDto>(dbEntity);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public IList<ProductBrandDto> GetAll()
        {
            try
            {
                List<ProductBrand> dbBrands = _dbContext.Brands.ToList();

                List<ProductBrandDto> brands = _mapper.Map<List<ProductBrand>, List<ProductBrandDto>>(dbBrands);

                return brands.OrderBy(x => x.Name).ToList(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductBrandDto Update(ProductBrandDto model)
        {
            try
            {
                ProductBrand dbEntity = RetrieveBrand(_dbContext, model);

                _dbContext.Entry(dbEntity).State = EntityState.Detached;

                dbEntity = _mapper.Map<ProductBrand>(model);

                _dbContext.Brands.Update(dbEntity);

                _dbContext.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public void Delete(ProductBrandDto model)
        {
            try
            {
                ProductBrand dbEntity = RetrieveBrand(_dbContext, model);

                _dbContext.Brands.Attach(dbEntity);

                _dbContext.Brands.Remove(dbEntity);

                _dbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        private void ValidateInputs(OzoneDbContext _dbContext, ProductBrandDto model)
        {
            // Check if ProductBrand was specified.
            if (model == null)
                throw new Exception($"No brand specified.");

            // Check uniqueness of brand
            if (_dbContext.Brands.Any(x => x.Name == model.Name))
                throw new Exception($"Brand with name '{ model.Name }' already exists");
        }

        private ProductBrand RetrieveBrand(OzoneDbContext _dbContext, ProductBrandDto model)
        {
            try
            {
                if (model == null)
                    throw new Exception("Brand not found");

                ProductBrand dbEntity = null;

                if (model.PkBrandId > 0)
                {
                    dbEntity = _dbContext.Brands.Where(x => x.PkBrandId == model.PkBrandId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(model.Name) && dbEntity == null)
                {
                    dbEntity = _dbContext.Brands.Where(x => x.Name == model.Name).FirstOrDefault();
                }

                if (dbEntity == null)
                    throw new Exception("Brand does not exists");

                return dbEntity;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}
