﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface IAttributeService
    {
        IList<ProductAttributeDto> GetAll();
        ProductAttributeDto Get(ProductAttributeDto model);
        ProductAttributeDto Create(ProductAttributeDto ProductAttribute);
        ProductAttributeDto Update(ProductAttributeDto ProductAttribute);
        void Delete(ProductAttributeDto ProductAttribute);
    }
}
