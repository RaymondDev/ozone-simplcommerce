﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface ICategoryService
    {
        IList<ProductCategoryDto> GetAll();
        ProductCategoryDto Get(ProductCategoryDto model);
        ProductCategoryDto Create(ProductCategoryDto ProductCategory);
        ProductCategoryDto Update(ProductCategoryDto ProductCategory);
        void Delete(ProductCategoryDto ProductCategory);
    }
}
