﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface IBrandService
    {
        IList<ProductBrandDto> GetAll();
        ProductBrandDto Get(ProductBrandDto model);
        ProductBrandDto Create(ProductBrandDto ProductBrand);
        ProductBrandDto Update(ProductBrandDto ProductBrand);
        void Delete(ProductBrandDto ProductBrand);
    }
}
