﻿using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Core.Services.ProductServices
{
    public class ProductService : IProductService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;

        public ProductService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public ProductDto Create(ProductDto model)
        {
            try
            {
                ValidateInputs(_dbContext, model);

                Product dbProduct = _mapper.Map<Product>(model);

                _dbContext.Products.Add(dbProduct);

                _dbContext.SaveChanges();

                model.PkProductId = dbProduct.PkProductId;

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductDto Get(ProductDto model)
        {
            try
            {
                Product dbEntity = RetrieveProduct(_dbContext, model);

                return _mapper.Map<ProductDto>(dbEntity);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public IList<ProductDto> GetAll()
        {
            try
            {
                List<Product> dbProductes = _dbContext.Products.ToList();

                List<ProductDto> taxClasses = _mapper.Map<List<Product>, List<ProductDto>>(dbProductes);

                // TODO: retrieve mappings

                return taxClasses.OrderBy(x => x.ProductName).ToList(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductDto Update(ProductDto model)
        {
            try
            {
                Product dbEntity = RetrieveProduct(_dbContext, model);

                _dbContext.Entry(dbEntity).State = EntityState.Detached;

                dbEntity = _mapper.Map<Product>(model);

                _dbContext.Products.Update(dbEntity);

                _dbContext.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public void Delete(ProductDto model)
        {
            try
            {
                Product dbEntity = RetrieveProduct(_dbContext, model);

                _dbContext.Products.Attach(dbEntity);

                _dbContext.Products.Remove(dbEntity);

                _dbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        private void ValidateInputs(OzoneDbContext _dbContext, ProductDto model)
        {
            // Check if Product was specified.
            if (model == null)
                throw new Exception($"No product specified.");

            // Check uniqueness of Productes
            if (_dbContext.Products.Any(x => x.ProductName == model.ProductName))
                throw new Exception($"Product with name '{ model.ProductName }' already exists");
        }

        private Product RetrieveProduct(OzoneDbContext _dbContext, ProductDto model)
        {
            try
            {
                if (model == null)
                    throw new Exception("Product not found");

                Product dbEntity = null;

                if (model.PkProductId > 0)
                {
                    dbEntity = _dbContext.Products.Where(x => x.PkProductId == model.PkProductId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(model.ProductName) && dbEntity == null)
                {
                    dbEntity = _dbContext.Products.Where(x => x.ProductName == model.ProductName).FirstOrDefault();
                }

                if (dbEntity == null)
                    throw new Exception("Product does not exists");

                return dbEntity;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}
