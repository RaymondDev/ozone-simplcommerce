﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Microsoft.EntityFrameworkCore;

namespace Core.Services.ProductServices
{
    public class PriceHistoryService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;
        public PriceHistoryService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public ProductPriceHistoryDto Create(ProductPriceHistoryDto model)
        {
            try
            {
                ProductPriceHistory dbPriceHistory = _mapper.Map<ProductPriceHistory>(model);

                _dbContext.ProductPriceHistory.Add(dbPriceHistory);

                _dbContext.SaveChanges();

                model.PkProductPriceHistoryId = dbPriceHistory.PkProductPriceHistoryId;

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductPriceHistoryDto Get(ProductPriceHistoryDto model)
        {
            try
            {
                ProductPriceHistory dbEntity = RetrievePriceHistory(_dbContext, model);

                return _mapper.Map<ProductPriceHistoryDto>(dbEntity);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public IList<ProductPriceHistoryDto> GetAll()
        {
            try
            {
                List<ProductPriceHistory> dbCategories = _dbContext.ProductPriceHistory.ToList();

                List<ProductPriceHistoryDto> priceHistorys = _mapper.Map<List<ProductPriceHistory>, List<ProductPriceHistoryDto>>(dbCategories);

                return priceHistorys.OrderBy(x => x.PkProductPriceHistoryId).ToList();
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductPriceHistoryDto Update(ProductPriceHistoryDto model)
        {
            try
            {
                ProductPriceHistory dbEntity = RetrievePriceHistory(_dbContext, model);

                _dbContext.Entry(dbEntity).State = EntityState.Detached;

                dbEntity = _mapper.Map<ProductPriceHistory>(model);

                _dbContext.ProductPriceHistory.Update(dbEntity);

                _dbContext.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public void Delete(ProductPriceHistoryDto model)
        {
            try
            {
                ProductPriceHistory dbEntity = RetrievePriceHistory(_dbContext, model);

                _dbContext.ProductPriceHistory.Attach(dbEntity);

                _dbContext.ProductPriceHistory.Remove(dbEntity);

                _dbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        private ProductPriceHistory RetrievePriceHistory(OzoneDbContext _dbContext, ProductPriceHistoryDto model)
        {
            try
            {
                if (model == null)
                    throw new Exception("Price history not found");

                ProductPriceHistory dbEntity = null;

                if (model.PkProductPriceHistoryId > 0)
                {
                    dbEntity = _dbContext.ProductPriceHistory
                        .Where(x => x.PkProductPriceHistoryId == model.PkProductPriceHistoryId)
                            .FirstOrDefault();
                }

                if (dbEntity == null)
                    throw new Exception("Price history does not exists");

                return dbEntity;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}
