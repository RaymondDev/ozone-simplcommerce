﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Microsoft.EntityFrameworkCore;

namespace Core.Services.ProductServices
{
    public class AttributeGroupService : IAttributeGroupService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;

        public AttributeGroupService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public ProductAttributeGroupDto Create(ProductAttributeGroupDto model)
        {
            try
            {
                ValidateInputs(_dbContext, model);

                ProductAttributeGroup dbAttributeGroup = _mapper.Map<ProductAttributeGroup>(model);

                _dbContext.AttributeGroups.Add(dbAttributeGroup);

                _dbContext.SaveChanges();

                model.PkAttributeGroupId = dbAttributeGroup.PkAttributeGroupId;

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductAttributeGroupDto Get(ProductAttributeGroupDto model)
        {
            try
            {
                ProductAttributeGroup dbEntity = RetrieveProductAttributeGroup(_dbContext, model);

                return _mapper.Map<ProductAttributeGroupDto>(dbEntity);
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public IList<ProductAttributeGroupDto> GetAll()
        {
            try
            {
                List<ProductAttributeGroup> dbAttributeGroups = _dbContext.AttributeGroups.ToList();

                List<ProductAttributeGroupDto> attributeGroups = _mapper.Map<List<ProductAttributeGroup>, List<ProductAttributeGroupDto>>(dbAttributeGroups);

                return attributeGroups.OrderBy(x => x.Name).ToList(); ;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public ProductAttributeGroupDto Update(ProductAttributeGroupDto model)
        {
            try
            {
                ProductAttributeGroup dbEntity = RetrieveProductAttributeGroup(_dbContext, model);

                _dbContext.Entry(dbEntity).State = EntityState.Detached;

                dbEntity = _mapper.Map<ProductAttributeGroup>(model);

                _dbContext.AttributeGroups.Update(dbEntity);

                _dbContext.SaveChanges();

                return model;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        public void Delete(ProductAttributeGroupDto model)
        {
            try
            {
                ProductAttributeGroup dbEntity = RetrieveProductAttributeGroup(_dbContext, model);

                _dbContext.AttributeGroups.Attach(dbEntity);

                _dbContext.AttributeGroups.Remove(dbEntity);

                _dbContext.SaveChanges();

            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

        private void ValidateInputs(OzoneDbContext _dbContext, ProductAttributeGroupDto ProductAttributeGroup)
        {
            // Check if ProductAttributeGroup was specified.
            if (ProductAttributeGroup == null)
                throw new Exception($"No ProductAttributeGroup specified.");

            // Check uniqueness of ProductAttributeGroup
            if (_dbContext.AttributeGroups.Any(x => x.Name == ProductAttributeGroup.Name))
                throw new Exception($"ProductAttributeGroup { ProductAttributeGroup.Name } already exists");
        }

        private ProductAttributeGroup RetrieveProductAttributeGroup(OzoneDbContext _dbContext, ProductAttributeGroupDto model)
        {
            try
            {
                if (model == null)
                    throw new Exception("ProductAttributeGroup not found");

                ProductAttributeGroup dbEntity = null;

                if (model.PkAttributeGroupId > 0)
                {
                    dbEntity = _dbContext.AttributeGroups.Where(x => x.PkAttributeGroupId == model.PkAttributeGroupId).FirstOrDefault();
                }

                if (!string.IsNullOrEmpty(model.Name) && dbEntity == null)
                {
                    dbEntity = _dbContext.AttributeGroups.Where(x => x.Name == model.Name).FirstOrDefault();
                }

                if (dbEntity == null)
                    throw new Exception("ProductAttributeGroup does not exists");

                return dbEntity;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }
    }
}
