﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Core.Domain;
using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using Core.Services.ProductAttributeMappingServices;

namespace Core.Services.ProductServices
{
    public class AttributeMappingService : IAttributeMappingService
    {
        private readonly OzoneDbContext _dbContext;
        private readonly IMapper _mapper;
        public AttributeMappingService(OzoneDbContext context, IMapper mapper)
        {
            _dbContext = context;
            _mapper = mapper;
        }

        public void AddOrDeleteAttributeMappings(List<ProductAttributeMappingDto> mappings)
        {
            Product dbProduct = null;

            if (mappings.Any())
            {
                int first = mappings.First().FkProductId;

                if (mappings.Any(x => x.FkProductId != first))
                {
                    throw new Exception("Inconsistencies found on productId");
                }

                dbProduct = _dbContext.Products
                    .Where(x => x.PkProductId == mappings.First().FkProductId)
                        .FirstOrDefault();
            }

            if (dbProduct != null)
            {
                List<ProductAttributeMapping> newMappings = new List<ProductAttributeMapping>();
                List<int> newMappingsAttributeIds = mappings.Select(x => x.FkAttributeId).ToList();

                List<ProductAttributeMapping> existingDbmappings = _dbContext.ProductAttributeMappings
                    .Where(x => x.FkProductId == dbProduct.PkProductId)
                        .ToList();

                List<ProductAttributeMapping> mappingsToDelete = existingDbmappings
                    .Where(x => !newMappingsAttributeIds.Contains(x.FkAttributeId)).ToList();

                List<int> existingIds = existingDbmappings.Select(x => x.FkAttributeId).ToList();


                foreach (var map in mappings)
                {
                    // Check if we already have this mapping
                    if (existingIds.Contains(map.FkAttributeId))
                    {
                        continue;
                    }

                    var mapping = new ProductAttributeMapping
                    {
                        FkAttributeId = map.FkAttributeId,
                        FkProductId = map.FkProductId,
                        Value = map.Value
                    };

                    newMappings.Add(mapping);
                }

                if (mappingsToDelete.Any())
                {
                    _dbContext.ProductAttributeMappings.RemoveRange(mappingsToDelete);
                }

                if (newMappings.Any())
                {
                    _dbContext.ProductAttributeMappings.AddRange(newMappings);
                }

                _dbContext.SaveChanges();
            }
        }

        public IList<ProductAttributeMappingDto> GetProductAttributeMappings(int productId)
        {
            try
            {
                Product dbProduct = null;
                List<ProductAttributeMapping> dbMappings = null;
                List<ProductAttributeMappingDto> mappings = null;

                // Validate if the product exists
                if (productId > 0)
                {
                    dbProduct = _dbContext.Products
                        .Where(x => x.PkProductId == productId)
                            .FirstOrDefault();
                }

                // Retrieve all product mappings
                if (dbProduct != null)
                {
                    dbMappings = _dbContext.ProductAttributeMappings
                        .Where(x => x.FkProductId == productId)
                            .ToList();
                }

                // Return mapping objects
                if (dbMappings.Any())
                {
                    Dictionary<int, string> dicAttributes = _dbContext.Attributes.ToDictionary(t => t.PkAttributeId, t => t.Name);

                    foreach (ProductAttributeMapping mapping in dbMappings)
                    {
                        ProductAttributeMappingDto map = new ProductAttributeMappingDto
                        {
                            PkProductMappingAttributeId = mapping.PkProductMappingAttributeId,
                            FkAttributeId = mapping.FkAttributeId,
                            Value = mapping.Value
                        };

                        if (dicAttributes.ContainsKey(mapping.FkAttributeId))
                        {
                            map.AttributeName = dicAttributes[mapping.FkAttributeId];
                        }

                        else
                        {
                            map.AttributeName = "N/A";
                        }

                        mappings.Add(map);
                    }
                }

                return mappings;
            }
            catch (Exception ex)
            {
                throw new Exception("Error: " + ex.Message);
            }
        }

    }
}
