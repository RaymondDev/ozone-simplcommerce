﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface IProductService
    {
        IList<ProductDto> GetAll();
        ProductDto Get(ProductDto model);
        ProductDto Create(ProductDto Product);
        ProductDto Update(ProductDto Product);
        void Delete(ProductDto Product);
    }
}
