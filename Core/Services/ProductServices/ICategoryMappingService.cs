﻿using Core.Domain.Entities.ProductEntity;
using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface ICategoryMappingService
    {
        IList<ProductCategoryMappingDto> GetProductCategoryMappings(int productId);
        void AddOrDeleteCategoryMappings(List<ProductCategoryMappingDto> mappings);
    }
}
