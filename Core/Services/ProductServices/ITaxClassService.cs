﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface ITaxClassService
    {
        IList<ProductTaxClassDto> GetAll();
        ProductTaxClassDto Get(ProductTaxClassDto model);
        ProductTaxClassDto Create(ProductTaxClassDto ProductTaxClass);
        ProductTaxClassDto Update(ProductTaxClassDto ProductTaxClass);
        void Delete(ProductTaxClassDto ProductTaxClass);
    }
}
