﻿using Core.Models.ProductModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Services.ProductServices
{
    public interface IPriceHistoryService
    {
        IList<ProductPriceHistoryDto> GetAll();
        ProductPriceHistoryDto Get(ProductPriceHistoryDto model);
        ProductPriceHistoryDto Create(ProductPriceHistoryDto ProductPriceHistory);
        ProductPriceHistoryDto Update(ProductPriceHistoryDto ProductPriceHistory);
        void Delete(ProductPriceHistoryDto ProductPriceHistory);
    }
}
