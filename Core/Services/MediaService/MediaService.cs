﻿using Core.Domain;
using Core.Domain.Entities.MediaEntity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Core.Services.MediaService
{
    public class MediaService : IMediaService
    {
        private OzoneDbContext _context;
        private IStorageService _storageService;

        public MediaService(OzoneDbContext context, IStorageService storageService)
        {
            _context = context;
            _storageService = storageService;
        }
    
        public string GetMediaUrl(Media media)
        {
            if (media == null)
            {
                return GetMediaUrl("no-image.png");
            }

            return GetMediaUrl(media.FileName);
        }

        public string GetMediaUrl(string fileName)
        {
            return _storageService.GetMediaUrl(fileName);
        }

        public string GetThumbnailUrl(Media media)
        {
            return GetMediaUrl(media);
        }

        public void SaveMedia(Stream mediaBinaryStream, string fileName, string mimeType = null)
        {
            _storageService.SaveMedia(mediaBinaryStream, fileName, mimeType);
        }

        public void DeleteMedia(Media media)
        {
            _context.Remove(media);
            DeleteMedia(media.FileName);
        }

        public void DeleteMedia(string fileName)
        {
            _storageService.DeleteMedia(fileName);
        }
    }
}
