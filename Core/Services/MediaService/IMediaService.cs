﻿using Core.Domain.Entities.MediaEntity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Core.Services.MediaService
{
    public interface IMediaService
    {
        string GetMediaUrl(Media media);

        string GetMediaUrl(string fileName);

        string GetThumbnailUrl(Media media);

        void SaveMedia(Stream mediaBinaryStream, string fileName, string mimeType = null);

        void DeleteMedia(Media media);

        void DeleteMedia(string fileName);
    }
}
