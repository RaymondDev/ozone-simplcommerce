﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Core.Services.MediaService
{
    public interface IStorageService
    {
        string GetMediaUrl(string fileName);

        void SaveMedia(Stream mediaBinaryStream, string fileName, string mimeType = null);

        void DeleteMedia(string fileName);
    }
}
