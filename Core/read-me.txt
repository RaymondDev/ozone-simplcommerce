﻿
Project Structure

This project is organised into the following folders:

Models (DTOs) - represent request and response models for controller methods, request models define the parameters for incoming requests, and response models can be used to define what data is returned.
Services - contains business logic, validation and data access code.
Entities - represent the application domain entities.
Helpers - anything that doesn't fit into the above folders.
Migrations - contains all your EF database access functionality and dbContexts