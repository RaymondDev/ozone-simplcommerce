﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ProductModels
{
    public class ProductAttributeMappingDto
    {
        public int PkProductMappingAttributeId { get; set; }

        public int FkAttributeId { get; set; }

        public string AttributeName { get; set; }

        public int FkProductId { get; set; }

        public string Value { get; set; }
    }
}
