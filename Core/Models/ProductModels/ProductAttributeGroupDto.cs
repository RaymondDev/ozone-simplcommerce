﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ProductModels
{
    public class ProductAttributeGroupDto
    {
        public int PkAttributeGroupId { get; set; }
        public string Name { get; set; }
    }
}
