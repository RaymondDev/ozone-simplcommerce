﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ProductModels
{
    public class ProductCategoryDto
    {
        public int PkCategoryId { get; set; }

        public int? FkParentId { get; set; }

        public int? FkMediaId { get; set; }

        public string Name { get; set; }

        public string Slug { get; set; }

        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsPublished { get; set; }

        public bool IncludeInMenu { get; set; }

        public bool IsDeleted { get; set; }
    }
}
