﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ProductModels
{
    public class ProductCategoryMappingDto_MirrorDB
    {
        public int PkProductMappingCategoryId { get; set; }

        public int FkCategoryId { get; set; }

        public int FkProductId { get; set; }

        public bool IsFeaturedProduct { get; set; }

        public int DisplayOrder { get; set; }
    }


    public class ProductCategoryMappingDto
    {
        public int PkProductMappingCategoryId { get; set; }

        public int FkCategoryId { get; set; }

        public string CategoryName { get; set; }

        public int FkProductId { get; set; }

        public bool IsFeaturedProduct { get; set; }

        public int DisplayOrder { get; set; }
    }
}
