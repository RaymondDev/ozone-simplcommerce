﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ProductModels
{
    public class ProductDto
    {
        public int PkProductId { get; set; }

        public int FkThumbnailImageId { get; set; }

        public int FkTaxClassId { get; set; }

        public int FkBrandId { get; set; }

        public string ProductName { get; set; }

        public string ProductSlug { get; set; }

        public DateTimeOffset? PublishedOn { get; set; }

        public string ShortDescription { get; set; }

        public string Description { get; set; }

        public string Specification { get; set; }

        public decimal Price { get; set; }

        public decimal? OldPrice { get; set; }

        public decimal? SpecialPrice { get; set; }

        public DateTimeOffset? SpecialPriceStart { get; set; }

        public DateTimeOffset? SpecialPriceEnd { get; set; }

        public string Sku { get; set; }

        public string Gtin { get; set; }

        public bool IsPublished { get; set; }

        public bool IsFeatured { get; set; }

        public bool IsCallForPricing { get; set; }

        public bool IsAllowToOrder { get; set; }

        public bool StockTrackingIsEnabled { get; set; }

        public int DisplayOrder { get; set; }

        public bool IsDeleted { get; set; }

        public List<ProductAttributeMappingDto> AttributeMappings { get; set; }

        public List<ProductAttributeMappingDto> CategoryMappings { get; set; }
    }
}
