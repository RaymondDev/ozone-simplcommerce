﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ProductModels
{
    public class ProductAttributeDto
    {
        public int PkAttributeId { get; set; }

        public int FkAttributeGroupId { get; set; }

        public string Name { get; set; }

        public string GroupName { get; set; }
    }
}
