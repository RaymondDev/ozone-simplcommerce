﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Models.ProductModels
{
    public class ProductTaxClassDto
    {
        public int PkTaxClassId { get; set; }

        public string Name { get; set; }
    }
}
