﻿
-------------------------------------
Migrations
-------------------------------------
Install the following or ensure that these packages are installed  

Install-Package Microsoft.EntityFrameworkCore.SqlServer -> Helps to translate c# objects into sql scripts using classes like (dbContext, DbSet, DbContextOptions).
Install-Package Microsoft.EntityFrameworkCore.Tools -> enables you to run the commands below.
Install-Package Microsoft.EntityFrameworkCore.Design -> contains all the sql design-time logic for Entity Framework Core.
	Add-Migration -> Adds a new migration
	Update-Database -> Updates the database to a specified migration
	Script-Migrations - generate SQL scripts
	Drop-Database -
	Get-DbContext
	Scaffold-DbContext

 Help
 get-help about_entityframeworkcore -> Provides entity framework core help

 Resources
 https://docs.microsoft.com/en-us/ef/core/managing-schemas/migrations/?tabs=dotnet-core-cli


Reference

 SYNTAX
    Add-Migration [-Name] <String> [-Force] [-ProjectName <String>] [-StartUpProjectName <String>] 
        [-ConfigurationTypeName <String>]     [-ConnectionStringName <String>] [-IgnoreChanges] 
        [-AppDomainBaseDirectory <String>] [<CommonParameters>]

-------------------------------------
Quick scripts
-------------------------------------
Add-Migration -Name Users
Update-Database

-------------------------------------
Reset migrations with 
-------------------------------------
Delete migration folder on the project
Delete database
Add-Migration -Name initial
Update-Database

OR

Remove the _MigrationHistory table from the Database
Remove the individual migration files in your project's Migrations folder
Enable-Migrations in Package Manager Console
Add-migration Initial in PMC
Comment out the code inside of the Up method in the Initial Migration
Update-database in PMC (does nothing but creates Migration Entry)
Remove comments in the Initial method