﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Domain.Entities.ProductEntity
{
    public class ProductCategoryMapping
    {
        [Key]
        public int PkProductMappingCategoryId { get; set; }

        public int FkCategoryId { get; set; }

        public int FkProductId { get; set; }

        public bool IsFeaturedProduct { get; set; }

        public int DisplayOrder { get; set; }
    }
}
