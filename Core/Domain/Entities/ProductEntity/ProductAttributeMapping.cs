﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Domain.Entities.ProductEntity
{
    public class ProductAttributeMapping
    {
        [Key]
        public int PkProductMappingAttributeId { get; set; }

        public int FkAttributeId { get; set; }

        public int FkProductId { get; set; }

        public string Value { get; set; }
    }
}
