﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Domain.Entities.ProductEntity
{
    public class ProductAttribute
    {
        [Key]
        public int PkAttributeId { get; set; }

        public int FkAttributeGroupId { get; set; }

        public string Name { get; set; }
    }
}
