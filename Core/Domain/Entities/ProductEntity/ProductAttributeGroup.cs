﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Core.Domain.Entities.ProductEntity
{
    public class ProductAttributeGroup
    {
        [Key]
        public int PkAttributeGroupId { get; set; }
        public string Name { get; set; }
    }
}
