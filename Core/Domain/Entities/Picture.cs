﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Entities
{
    /// <summary>
    /// Represents a picture
    /// </summary>
    public class Picture
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public int PkPictureId { get; set; }

        /// <summary>
        /// Gets or sets the picture mime type
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// Gets or sets the SEO friendly filename of the picture
        /// </summary>
        public string SeoFilename { get; set; }

        /// <summary>
        /// Gets or sets the picture binary
        /// </summary>
        public virtual PictureBinary PictureBinary { get; set; }
    }
}
