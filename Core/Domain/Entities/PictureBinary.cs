﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Entities
{
    /// <summary>
    /// Represents a picture binary data
    /// </summary>
    public class PictureBinary
    {
        /// <summary>
        /// Gets or sets the entity identifier
        /// </summary>
        public int PkPictureBinaryId { get; set; }

        /// <summary>
        /// Gets or sets the picture binary
        /// </summary>
        public byte[] BinaryData { get; set; }

        /// <summary>
        /// Gets or sets the picture identifier
        /// </summary>
        public int PictureId { get; set; }

        /// <summary>
        /// Gets or sets the picture
        /// </summary>
        public virtual Picture Picture { get; set; }
    }
}
