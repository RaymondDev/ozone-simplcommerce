﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Entities.MediaEntity
{
    public enum MediaType
    {
        Image = 1,

        File = 5,

        Video = 10,
    }
}
