﻿using Core.Domain.Entities;
using Core.Domain.Entities.ProductEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Domain.Config
{
    public class CategoryConfig : IEntityTypeConfiguration<ProductCategory>
    {
        public void Configure(EntityTypeBuilder<ProductCategory> builder)
        {
            builder.ToTable(nameof(ProductCategory));
            builder.HasKey(Category => Category.PkCategoryId);

            builder.Property(Category => Category.Name).HasMaxLength(450).IsRequired();
            builder.Property(Category => Category.Slug).HasMaxLength(450).IsRequired();
         }
    }
}
