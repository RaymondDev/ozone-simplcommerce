﻿using System;
using System.Collections.Generic;
using System.Text;
using Core.Domain.Entities;
using Core.Domain.Entities.ProductEntity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Core.Domain.Config
{
    public class BrandConfig : IEntityTypeConfiguration<ProductBrand>
    {
        public void Configure(EntityTypeBuilder<ProductBrand> builder)
        {
            builder.ToTable(nameof(ProductBrand));
            builder.HasKey(Brand => Brand.PkBrandId);

            builder.Property(Brand => Brand.Name).HasMaxLength(450).IsRequired();
            builder.Property(Brand => Brand.Slug).HasMaxLength(450).IsRequired();
        }
    }
}
